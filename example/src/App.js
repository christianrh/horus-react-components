import React, { Component } from 'react'

import { Button } from 'horus-react-components'

export default class App extends Component {
  render () {
    return (
      <div>
        <Button />
      </div>
    )
  }
}
